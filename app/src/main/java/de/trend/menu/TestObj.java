package de.trend.menu;


import com.google.auto.value.AutoValue;

@AutoValue
public abstract class TestObj {
    public abstract String firstname();

    public abstract String lastname();

    public static TestObj create(String firstname, String lastname) {
        return builder()
                .firstname(firstname)
                .lastname(lastname)
                .build();
    }

    public static Builder builder() {
        return new AutoValue_TestObj.Builder();
    }


    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder firstname(String firstname);

        public abstract Builder lastname(String lastname);

        public abstract TestObj build();
    }
}