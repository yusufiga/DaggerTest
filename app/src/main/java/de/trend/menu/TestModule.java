package de.trend.menu;

import dagger.Module;
import dagger.Provides;

@Module
public class TestModule {
    @Provides
    TestHeater provideTestHeater(){
        return new TestHeater();
    }
}
