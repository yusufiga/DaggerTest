package de.trend.menu;

import android.util.Log;

import javax.inject.Inject;

/**
 * Created by Yusuf Figanioğlu on 17/09/16.
 */
public class GoogleAnalyticsCollector implements  AnalyticsCollector{
    private final Heater heater;
    private final TestHeater theater;

    @Inject
    public GoogleAnalyticsCollector(Heater heater,TestHeater theater) {
        this.heater = heater;
        this.theater = theater;
    }

    @Override
    public void sendRecord(String key) {


        Log.d(heater.getTitle(), theater.getTitle());
    }
}
