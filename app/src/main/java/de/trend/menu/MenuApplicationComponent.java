package de.trend.menu;

import android.app.Activity;

import dagger.Component;

/**
 * Created by Yusuf Figanioğlu on 17/09/16.
 */
@Component(modules = {ApplicationModule.class,TestModule.class})
public interface MenuApplicationComponent {

    void inject(MainActivity activity);
}
