package de.trend.menu;

/**
 * Created by Yusuf Figanioğlu on 17/09/16.
 */
public interface AnalyticsCollector {

    void sendRecord(String key);

}
