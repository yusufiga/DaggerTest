package de.trend.menu;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Yusuf Figanioğlu on 17/09/16.
 */
@Module
public abstract class ApplicationModule {

//    @Provides
//    AnalyticsCollector provideAnalyticsCollector(Heater heater, TestHeater testHeater){
//        return new GoogleAnalyticsCollector(heater,testHeater);
//    }

    @Binds
    public abstract AnalyticsCollector bindTestHeater(GoogleAnalyticsCollector testHeater);



}
