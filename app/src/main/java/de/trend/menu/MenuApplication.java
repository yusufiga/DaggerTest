package de.trend.menu;

import android.app.Application;
import android.content.Context;

/**
 * Created by Yusuf Figanioğlu on 17/09/16.
 */
public class MenuApplication extends Application {


    private static MenuApplicationComponent component;

    public static void inject(Object target) {
        Dagger2Helper.inject(MenuApplicationComponent.class, component, target);
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        component = Dagger2Helper.buildComponent(MenuApplicationComponent.class, ApplicationModule.class, TestModule.class);
    }
}
