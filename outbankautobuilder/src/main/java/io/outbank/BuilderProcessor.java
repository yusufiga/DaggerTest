package io.outbank;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.tools.JavaFileObject;

import io.outbank.processors.Builder;


@SupportedAnnotationTypes("io.outbank.processors.Builder")
@SupportedSourceVersion(SourceVersion.RELEASE_7)
public class BuilderProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {


        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(Builder.class);


        StringBuilder builder = new StringBuilder()
                .append("package io.outbank.processors.generated;\n\n")
                .append("public class GeneratedClass {\n\n"); // open class


        // for each javax.lang.model.element.Element annotated with the CustomAnnotation
        for (Element element : elements) {

            if (element.getKind() == ElementKind.CLASS) {

                TypeElement clazz = (TypeElement) element;
                System.out.println(clazz.getSimpleName().toString());
                for (Element varE : clazz.getEnclosedElements()) {
                    if (varE.getKind() == ElementKind.FIELD) {
                        VariableElement var = (VariableElement) varE;
                        System.out.println(var.asType().toString());
                        System.out.println(var.getSimpleName());
                    }
                }


//                System.out.println(element.getEnclosedElements().get(0)..getDeclaringClass().getSimpleName());
//
//                    builder.append(BuildHelper(c));
//                    Field f = c.getField
//                    System.out.format("Type: %s%n", f.getType());
//                    System.out.format("GenericType: %s%n", f.getGenericType());

//                     production code should handle these exceptions more gracefully
            }
            // builder.append(BuildHelper(element.getEnclosedElements().get(0).getEnclosingElement()));
        }


        builder.append("\n\n}"); // close class


        try { // write the file
            JavaFileObject source = processingEnv.getFiler().createSourceFile("io.outbank.processors.generated.GeneratedClass");


            Writer writer = source.openWriter();
            writer.write(builder.toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            // Note: calling e.printStackTrace() will print IO errors
            // that occur from the file already existing after its first run, this is normal
        }


        return true;
    }

    private String BuildHelper(Class<?> aClass) {

        StringBuilder builder = new StringBuilder()
                .append("public static final class Builder {\n\n"); // open class


        Field[] fields = aClass.getDeclaredFields();

        for (Field field : fields) {
            builder.append("\tprivate " + field.getType() + " " + field.getName() + ";\n\n");
        }

        builder.append("\tpublic Builder() {\n\t}\n");

        for (Field field : fields) {
            builder.append("\n\n\tpublic Builder " + field.getName() + "(" + field.getType() + " val) {\n\t\t" +
                    field.getName() + " = val;\n\t\t return this;\n\t}");
        }

        builder.append("\n\tpublic " + aClass.getName() + " build() {\n\t\treturn new " + aClass.getName() + "(this);\n\t}");
        builder.append("}");

        return builder.toString();
    }
}
